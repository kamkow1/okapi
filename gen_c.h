#ifndef OKAPI_GEN_C_H
#define OKAPI_GEN_C_H

#include "mibs.h"
#include "parser.h"

Mibs_String_Builder okapi_generate_c(Okapi_Stmts* stmts);
void okapi_generate_c_stmt(Mibs_String_Builder* sb, Okapi_Stmt* stmt, size_t indent);
void okapi_generate_c_vardef_stmt(Mibs_String_Builder* sb, Okapi_Stmt* stmt);
void okapi_generate_c_funcdef_stmt(Mibs_String_Builder* sb, Okapi_Stmt* stmt);
void okapi_generate_c_return_stmt(Mibs_String_Builder* sb, Okapi_Stmt* stmt);
void okapi_generate_c_cimport_stmt(Mibs_String_Builder* sb, Okapi_Stmt* stmt);
void okapi_generate_c_type(Mibs_String_Builder* sb, Okapi_Type* type);
void okapi_generate_c_expr(Mibs_String_Builder* sb, Okapi_Expr* expr);
void okapi_generate_c_int_expr(Mibs_String_Builder* sb, Okapi_Expr* expr);
void okapi_generate_c_float_expr(Mibs_String_Builder* sb, Okapi_Expr* expr);
void okapi_generate_c_string_expr(Mibs_String_Builder* sb, Okapi_Expr* expr);
void okapi_generate_c_binop_expr(Mibs_String_Builder* sb, Okapi_Expr* expr);
void okapi_generate_c_emph_expr(Mibs_String_Builder* sb, Okapi_Expr* expr);
void okapi_generate_c_call_expr(Mibs_String_Builder* sb, Okapi_Expr* expr);
void okapi_generate_c_ident_expr(Mibs_String_Builder* sb, Okapi_Expr* expr);

#endif // OKAPI_GEN_C_H
