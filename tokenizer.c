#include <ctype.h>
#include "allocator_config.h"
#include "mibs.h"
#include "allocator.h"
#include "tokenizer.h"

const char* string_tokens[] = {
    [OKAPI_TK_INT] = "integer",
    [OKAPI_TK_IDENT] = "identifier",
    [OKAPI_TK_FLOAT] = "float",
    [OKAPI_TK_STRING] = "string",
    [OKAPI_TK_RETURN] = "return",
    [OKAPI_TK_FUNCTION] = "function",
    [OKAPI_TK_VARIABLE] = "variable",
    [OKAPI_TK_CIMPORT] = "c import",
    ['['] = "[",
    [']'] = "]",
    [':'] = ":",
    [';'] = ";",
    ['{'] = "{",
    ['}'] = "}",
    ['='] = "=",
    ['*'] = "*",
    ['+'] = "+",
    ['/'] = "/",
    ['-'] = "-",
    ['('] = "(",
    [')'] = ")",
};
    
static const char* single_char_tokens = "[]{};:fv=*+/-()";

static const char* keywords[] = {
    "cimport",
    "return",
};

bool okapi_is_keyword(const char* s)
{
    for (size_t i = 0; i < sizeof(keywords)/sizeof(const char*); i++) {
        if (mibs_compare_cstr(keywords[i], s)) return true;
    }
    return false;
}

bool okapi_is_single_char_token(char c)
{
    for (size_t j = 0; j < strlen(single_char_tokens); j++) {
        if (single_char_tokens[j] == c) {
            return true;
        }
    }
    return false;
}

Okapi_Tokens okapi_tokenize(const char* text, size_t len)
{
    Okapi_Tokens tokens = {0};

    size_t line = 1;
    size_t column = 1;

    for (size_t i = 0; i < len; i++) {
        if (isspace(text[i]) && text[i] != '\n') {
            column++;
        } else if (text[i] == '\n') {
            line++;
            column = 1;
        } else if (isalpha(text[i]) && !okapi_is_single_char_token(text[i]) || text[i] == '_') {
            Okapi_Token token = {
                .line = line,
                .column = column,
                .text = {0},
                .kind = OKAPI_TK_IDENT,
            };
            while(isalnum(text[i]) || text[i] == '_') {
                mibs_sb_append_char(&allocator, &token.text, text[i]);
                i++;
                column++;
            }
            i--;
            mibs_sb_append_null(&allocator, &token.text);

            if (okapi_is_keyword(token.text.items)) {
                if (mibs_compare_cstr(token.text.items, "cimport")) {
                    token.kind = OKAPI_TK_CIMPORT;
                } else if (mibs_compare_cstr(token.text.items, "return")) {
                    token.kind = OKAPI_TK_RETURN;
                }
            }

            mibs_da_append(&allocator, &tokens, token);
        } else if (isdigit(text[i])) {
            Okapi_Token token = {
                .line = line,
                .column = column,
                .text = {0},
                .kind = OKAPI_TK_INT,
            };
            while(isdigit(text[i]) || text[i] == '.') {
                if (text[i] == '.') {
                    token.kind = OKAPI_TK_FLOAT;
                }
                mibs_sb_append_char(&allocator, &token.text, text[i]);
                i++;
                column++;
            }
            i--;
            mibs_sb_append_null(&allocator, &token.text);
            mibs_da_append(&allocator, &tokens, token);
        } else if (text[i] == '"') {
            Okapi_Token token = {
                .line = line,
                .column = column,
                .text = {0},
                .kind = OKAPI_TK_STRING,
            };
            i++;
            column++;
            while(text[i] != '"') {
                mibs_sb_append_char(&allocator, &token.text, text[i]);
                i++;
                column++;
            }
            mibs_sb_append_null(&allocator, &token.text);
            mibs_da_append(&allocator, &tokens, token);
        } else {
            bool found = false;
            for (size_t j = 0; j < strlen(single_char_tokens); j++) {
                if (single_char_tokens[j] == text[i]) {
                    found = true;
                    Okapi_Token token = {
                        .line = line,
                        .column = column,
                        .text = {0},
                        .kind = single_char_tokens[j],
                    };
                    column++;
                    mibs_sb_append_char(&allocator, &token.text, text[i]);
                    mibs_sb_append_null(&allocator, &token.text);
                    mibs_da_append(&allocator, &tokens, token);
                }
            }
            if (!found && text[i] != 0) {
                mibs_log(MIBS_LL_ERROR, "%zu:%zu Unknown token %c\n", line, column, text[i]);
                column++;
            }
        }
    }

    return tokens;
}
