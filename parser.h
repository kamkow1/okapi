#ifndef OKAPI_PARSER_H
#define OKAPI_PARSER_H

#include "mibs.h"
#include "tokenizer.h"

typedef struct Okapi_Expr Okapi_Expr;

typedef Mibs_Da(Okapi_Expr*) Okapi_Exprs;

typedef enum {
    OKAPI_EK_INT,
    OKAPI_EK_FLOAT,
    OKAPI_EK_STRING,
    OKAPI_EK_BINOP,
    OKAPI_EK_EMPH,
    OKAPI_EK_CALL,
    OKAPI_EK_IDENT,
} Okapi_Expr_Kind;

typedef struct {
    long value;
} Okapi_Int_Expr;

typedef struct {
    double value;
} Okapi_Float_Expr;

typedef struct {
    const char* value;
} Okapi_String_Expr;

typedef struct {
    Okapi_Expr* lhs;
    Okapi_Expr* rhs;
    char op;
} Okapi_Binop_Expr;

typedef struct {
    Okapi_Expr* underlying;
} Okapi_Emph_Expr;

typedef struct {
    Okapi_Exprs *args;
    const char* name;
} Okapi_Call_Expr;

typedef struct {
    const char* name;
} Okapi_Ident_Expr;

typedef union {
    Okapi_Int_Expr integer;
    Okapi_Float_Expr floating_point;
    Okapi_String_Expr string;
    Okapi_Binop_Expr binop;
    Okapi_Emph_Expr emph;
    Okapi_Call_Expr call;
    Okapi_Ident_Expr ident;
} Okapi_Expr_As;

struct Okapi_Expr {
    Okapi_Expr_Kind kind;
    Okapi_Expr_As as;
};

typedef struct Okapi_Stmt Okapi_Stmt;

typedef Mibs_Da(Okapi_Stmt*) Okapi_Stmts;

typedef enum {
    OKAPI_SK_FUNCDEF,
    OKAPI_SK_VARDEF,
    OKAPI_SK_RETURN,
    OKAPI_SK_EXPR,
    OKAPI_SK_CIMPORT,
} Okapi_Stmt_Kind;

typedef struct {
    const char* base_name;
    size_t ptr_count;
} Okapi_Type;

typedef struct {
    const char* name;
    Okapi_Type* type;
} Okapi_Func_Param;

typedef Mibs_Da(Okapi_Func_Param) Okapi_Func_Params;

typedef struct {
    const char* name;
    Okapi_Func_Params *params;
    Okapi_Stmts* stmts;
    Okapi_Type* return_type;
} Okapi_Funcdef_Stmt;

typedef struct {
    const char* name;
    Okapi_Type* type;
    Okapi_Expr* expr;
} Okapi_Vardef_Stmt;

typedef struct {
    Okapi_Expr* expr;
} Okapi_Return_Stmt;

typedef struct {
    Okapi_Expr* expr;
} Okapi_Expr_Stmt;

typedef struct {
    const char* path;
} Okapi_CImport_Stmt;

typedef union {
    Okapi_Funcdef_Stmt funcdef;
    Okapi_Vardef_Stmt vardef;
    Okapi_Return_Stmt retrn;
    Okapi_Expr_Stmt expr;
    Okapi_CImport_Stmt cimport;
} Okapi_Stmt_As;

struct Okapi_Stmt {
    Okapi_Stmt_Kind kind;
    Okapi_Stmt_As as;
};

Okapi_Token* okapi_expect_token(Okapi_Token_Kind tk, Okapi_Tokens tokens, size_t* i);

Okapi_Func_Params* okapi_parse_func_params(Okapi_Tokens tokens, size_t *i);
Okapi_Stmt* okapi_parse_funcdef_stmt(Okapi_Tokens tokens, size_t *i);
Okapi_Stmt* okapi_parse_vardef_stmt(Okapi_Tokens tokens, size_t *i);
Okapi_Stmt* okapi_parse_return_stmt(Okapi_Tokens tokens, size_t *i);
Okapi_Stmt* okapi_parse_cimport_stmt(Okapi_Tokens tokens, size_t *i);
Okapi_Stmt* okapi_parse_expr_stmt(Okapi_Tokens tokens, size_t *i);
Okapi_Stmt* okapi_parse_stmt(Okapi_Tokens tokens, size_t *i);
Okapi_Stmts *okapi_parse_stmts(Okapi_Tokens tokens, size_t *i);
Okapi_Type* okapi_parse_type(Okapi_Tokens tokens, size_t *i);
Okapi_Expr* okapi_parse_expr(Okapi_Tokens tokens, size_t *i);
Okapi_Expr* okapi_parse_int_expr(Okapi_Tokens tokens, size_t* i);
Okapi_Expr* okapi_parse_float_expr(Okapi_Tokens tokens, size_t* i);
Okapi_Expr* okapi_parse_string_expr(Okapi_Tokens tokens, size_t* i);
Okapi_Expr* okapi_parse_binop_expr(Okapi_Tokens tokens, size_t* i);
Okapi_Expr* okapi_parse_emph_expr(Okapi_Tokens tokens, size_t* i);
Okapi_Expr* okapi_parse_expr_with_ident(Okapi_Tokens tokens, size_t* i);
Okapi_Expr* okapi_parse_call_expr(Okapi_Tokens tokens, size_t* i);
Okapi_Expr* okapi_parse_ident_expr(Okapi_Tokens tokens, size_t* i);
Okapi_Expr* okapi_parse_call_expr(Okapi_Tokens tokens, size_t* i);

#endif // OKAPI_PARSER_H
