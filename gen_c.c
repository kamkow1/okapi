#include <math.h>
#include "allocator_config.h"
#include "mibs.h"
#include "parser.h"
#include "allocator.h"
#include "gen_c.h"

void okapi_generate_c_type(Mibs_String_Builder* sb, Okapi_Type* type)
{
    mibs_sb_append_cstr(&allocator, sb, type->base_name);
    for (size_t i = 0; i < type->ptr_count; i++) {
        mibs_sb_append_char(&allocator, sb, '*');
    }
}

void okapi_generate_c_funcdef_stmt(Mibs_String_Builder* sb, Okapi_Stmt* stmt)
{
    Okapi_Funcdef_Stmt funcdef = stmt->as.funcdef;

    // return type
    okapi_generate_c_type(sb, funcdef.return_type); 
    mibs_sb_append_char(&allocator, sb, ' ');

    // name
    mibs_sb_append_cstr(&allocator, sb, funcdef.name);
    mibs_sb_append_char(&allocator, sb, ' ');

    // params
    mibs_sb_append_char(&allocator, sb, '(');
    for (size_t i = 0; i < funcdef.params->count; i++) {
        okapi_generate_c_type(sb, funcdef.params->items[i].type);
        mibs_sb_append_char(&allocator, sb, ' ');
        mibs_sb_append_cstr(&allocator, sb, funcdef.params->items[i].name);
        if (i < funcdef.params->count-1) {
            mibs_sb_append_cstr(&allocator, sb, ", ");
        }
    }
    mibs_sb_append_char(&allocator, sb, ')');

    mibs_sb_append_char(&allocator, sb, '{');
    mibs_sb_append_char(&allocator, sb, '\n');

    for (size_t i = 0; i < funcdef.stmts->count; i++) {
        okapi_generate_c_stmt(sb, funcdef.stmts->items[i], 1);
    }

    mibs_sb_append_char(&allocator, sb, '}');
    mibs_sb_append_char(&allocator, sb, '\n');
}

void okapi_generate_c_vardef_stmt(Mibs_String_Builder* sb, Okapi_Stmt* stmt)
{
    Okapi_Vardef_Stmt vardef = stmt->as.vardef;
    
    okapi_generate_c_type(sb, vardef.type);    
    mibs_sb_append_char(&allocator, sb, ' ');
    mibs_sb_append_cstr(&allocator, sb, vardef.name);
    mibs_sb_append_char(&allocator, sb, ' ');
    mibs_sb_append_char(&allocator, sb, '=');
    mibs_sb_append_char(&allocator, sb, ' ');
    okapi_generate_c_expr(sb, vardef.expr);
    mibs_sb_append_char(&allocator, sb, ';');

    mibs_sb_append_char(&allocator, sb, '\n');
}

void okapi_generate_c_return_stmt(Mibs_String_Builder* sb, Okapi_Stmt* stmt)
{
    Okapi_Return_Stmt retrn = stmt->as.retrn;

    mibs_sb_append_cstr(&allocator, sb, "return");
    mibs_sb_append_char(&allocator, sb, ' ');
    okapi_generate_c_expr(sb, retrn.expr);
    mibs_sb_append_char(&allocator, sb, ';');
    mibs_sb_append_char(&allocator, sb, '\n');
}

void okapi_generate_c_cimport_stmt(Mibs_String_Builder* sb, Okapi_Stmt* stmt)
{
    mibs_sb_append_cstr(&allocator, sb, "#include ");
    mibs_sb_append_char(&allocator, sb, '"');
    mibs_sb_append_cstr(&allocator, sb, stmt->as.cimport.path);
    mibs_sb_append_char(&allocator, sb, '"');
    mibs_sb_append_char(&allocator, sb, '\n');
}

void okapi_generate_c_stmt(Mibs_String_Builder* sb, Okapi_Stmt* stmt, size_t indent)
{
    for (size_t i = 0; i < indent*4; i++) {
        mibs_sb_append_char(&allocator, sb, ' ');
    }
    switch (stmt->kind) {
        case OKAPI_SK_FUNCDEF: okapi_generate_c_funcdef_stmt(sb, stmt); break;
        case OKAPI_SK_VARDEF: okapi_generate_c_vardef_stmt(sb, stmt); break;
        case OKAPI_SK_RETURN: okapi_generate_c_return_stmt(sb, stmt); break;
        case OKAPI_SK_CIMPORT: okapi_generate_c_cimport_stmt(sb, stmt); break;
        case OKAPI_SK_EXPR: {
            okapi_generate_c_expr(sb, stmt->as.expr.expr);
            mibs_sb_append_cstr(&allocator, sb, ";\n");
        } break;
        default: mibs_log(MIBS_LL_ERROR, "%zu:%zu %s: Unhandled case in okapi_generate_c_stmt()!\n"); break;
    }
}

void okapi_generate_c_int_expr(Mibs_String_Builder* sb, Okapi_Expr* expr)
{
    size_t size = snprintf(NULL, 0, "%ld", expr->as.integer.value)+1;
    const char* str = (const char*)mibs_alloc(&allocator, size);
    sprintf((char*)str, "%ld", expr->as.integer.value);
    mibs_sb_append_cstr(&allocator, sb, str);
}

void okapi_generate_c_float_expr(Mibs_String_Builder* sb, Okapi_Expr* expr)
{
    size_t size = snprintf(NULL, 0, "%lf", expr->as.floating_point.value)+1;
    const char* str = (const char*)mibs_alloc(&allocator, size);
    sprintf((char*)str, "%lf", expr->as.floating_point.value);
    mibs_sb_append_cstr(&allocator, sb, str);
}

void okapi_generate_c_string_expr(Mibs_String_Builder* sb, Okapi_Expr* expr)
{
    mibs_sb_append_char(&allocator, sb, '"');
    mibs_sb_append_cstr(&allocator, sb, expr->as.string.value);
    mibs_sb_append_char(&allocator, sb, '"');
}

void okapi_generate_c_binop_expr(Mibs_String_Builder* sb, Okapi_Expr* expr)
{
    mibs_sb_append_char(&allocator, sb, '(');
    okapi_generate_c_expr(sb, expr->as.binop.lhs);
    mibs_sb_append_char(&allocator, sb, expr->as.binop.op);
    okapi_generate_c_expr(sb, expr->as.binop.rhs);
    mibs_sb_append_char(&allocator, sb, ')');
}

void okapi_generate_c_emph_expr(Mibs_String_Builder* sb, Okapi_Expr* expr)
{
    mibs_sb_append_char(&allocator, sb, '(');
    okapi_generate_c_expr(sb, expr->as.emph.underlying);
    mibs_sb_append_char(&allocator, sb, ')');
}

void okapi_generate_c_call_expr(Mibs_String_Builder* sb, Okapi_Expr* expr)
{
    mibs_sb_append_char(&allocator, sb, '(');
    mibs_sb_append_cstr(&allocator, sb, expr->as.call.name);
    mibs_sb_append_char(&allocator, sb, '(');
    for (size_t i = 0; i < expr->as.call.args->count; i++) {
        okapi_generate_c_expr(sb, expr->as.call.args->items[i]);
        if (i < expr->as.call.args->count - 1) {
            mibs_sb_append_char(&allocator, sb, ',');
        }
    }
    mibs_sb_append_char(&allocator, sb, ')');
    mibs_sb_append_char(&allocator, sb, ')');
}

void okapi_generate_c_ident_expr(Mibs_String_Builder* sb, Okapi_Expr* expr)
{
    mibs_sb_append_char(&allocator, sb, '(');
    mibs_sb_append_cstr(&allocator, sb, expr->as.ident.name);
    mibs_sb_append_char(&allocator, sb, ')');
}

void okapi_generate_c_expr(Mibs_String_Builder* sb, Okapi_Expr* expr)
{
    switch (expr->kind) {
        case OKAPI_EK_INT: okapi_generate_c_int_expr(sb, expr); break;
        case OKAPI_EK_FLOAT: okapi_generate_c_float_expr(sb, expr); break;
        case OKAPI_EK_STRING: okapi_generate_c_string_expr(sb, expr); break;
        case OKAPI_EK_BINOP: okapi_generate_c_binop_expr(sb, expr); break;
        case OKAPI_EK_EMPH: okapi_generate_c_emph_expr(sb, expr); break;
        case OKAPI_EK_CALL: okapi_generate_c_call_expr(sb, expr); break;
        case OKAPI_EK_IDENT: okapi_generate_c_ident_expr(sb, expr); break;
        default: mibs_log(MIBS_LL_ERROR, "%zu:%zu %s: Unhandled case in okapi_generate_c_expr()!\n"); break;
    }
}

Mibs_String_Builder okapi_generate_c(Okapi_Stmts* stmts)
{
    Mibs_String_Builder sb = {0};

    for (size_t i = 0; i < stmts->count; i++) {
        okapi_generate_c_stmt(&sb, stmts->items[i], 0);
    }

    mibs_sb_append_null(&allocator, &sb);
    return sb;
}

