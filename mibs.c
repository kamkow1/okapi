#define MIBS_LINEAR_CAP 20*1024

#define MIBS_IMPL
#include "mibs.h"

Mibs_Default_Allocator allocator = mibs_make_default_allocator();

#define src \
    "okapi.c", \
    "allocator.c", \
    "tokenizer.c", \
    "parser.c", \
    "gen_c.c"

void generate_lang_header(void)
{
    mibs_log(MIBS_LL_INFO, "Generating lang_header.h\n");

    Mibs_Read_File_Result h_result = mibs_read_file(&allocator, "lang.h");
    Mibs_String_Builder h = h_result.value;
    Mibs_String_Builder actual_header = {0};
    mibs_sb_append_cstr(&allocator, &actual_header, "#ifndef OKAPI_LANG_H_ACTUAL\n");
    mibs_sb_append_cstr(&allocator, &actual_header, "#define OKAPI_LANG_H_ACTUAL\n");
    
    mibs_sb_append_cstr(&allocator, &actual_header, "static const char lang_header[] = {");
    for (size_t i = 0; i < h.count-1; i++) {
        mibs_sb_append_char(&allocator, &actual_header, '\'');
        switch(h.items[i]) {
            case '\n':
                mibs_sb_append_cstr(&allocator, &actual_header, "\\n");
                break;
            case '\r':
                mibs_sb_append_cstr(&allocator, &actual_header, "\\r");
                break;
            default:
                mibs_sb_append_char(&allocator, &actual_header, h.items[i]);
                break;
        }
        mibs_sb_append_char(&allocator, &actual_header, '\'');
        if (i < h.count-1) {
            mibs_sb_append_char(&allocator, &actual_header, ',');
        }
    }
    mibs_sb_append_cstr(&allocator, &actual_header, "};\n");

    mibs_sb_append_cstr(&allocator, &actual_header, "#endif // OKAPI_LANG_H_ACTUAL\n");

    mibs_write_file(&allocator, "lang_header.h", actual_header.items, actual_header.count, MIBS_FK_TEXT, MIBS_FM_OVERWRITE);
    
    mibs_log(MIBS_LL_INFO, "Done generating lang_header.h\n");
}

int main(int argc, char** argv)
{
    mibs_rebuild(&allocator, argc, argv);

    generate_lang_header();

    Mibs_Cmd cmd = {0};
    mibs_cmd_append(&allocator, &cmd, MIBS_CC);
    mibs_cmd_append(&allocator, &cmd, "-ggdb", "-o", "okapi", src);
    mibs_cmd_append(&allocator, &cmd, "-lm");
    if (!mibs_run_cmd(&allocator, &cmd, MIBS_CMD_SYNC, NULL).ok) return 1;

    return 0;
}
