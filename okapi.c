#include "allocator_config.h"
#define MIBS_IMPL
#include "mibs.h"
#include "allocator.h"
#include "tokenizer.h"
#include "parser.h"
#include "gen_c.h"
#include "lang_header.h"

void print_expr(Okapi_Expr* expr, size_t indent)
{
    switch(expr->kind) {
        case OKAPI_EK_INT: {
            mibs_log(MIBS_LL_INFO, "%*sint expr %ld\n", indent*4, "", expr->as.integer.value);
        } break;
        case OKAPI_EK_FLOAT: {
            mibs_log(MIBS_LL_INFO, "%*sfloat expr %lf\n", indent*4, "", expr->as.floating_point.value);
        } break;
        case OKAPI_EK_STRING: {
            mibs_log(MIBS_LL_INFO, "%*sstring expr %s\n", indent*4, "", expr->as.string.value);
        } break;
        case OKAPI_EK_BINOP: {
            mibs_log(MIBS_LL_INFO, "%*sbinop expr %c\n", indent*4, "", expr->as.binop.op);
            print_expr(expr->as.binop.lhs, indent+1);
            print_expr(expr->as.binop.rhs, indent+1);
        } break;
        case OKAPI_EK_CALL: {
            mibs_log(MIBS_LL_INFO,"%*scall expr %s\n", indent*4, "", expr->as.call.name);
            for (size_t i = 0; i < expr->as.call.args->count; i++) {
                print_expr(expr->as.call.args->items[i], indent+1);
            } 
        } break;
        case OKAPI_EK_IDENT: {
            mibs_log(MIBS_LL_INFO, "%*sident expr %s\n", indent*4, "", expr->as.ident.name);
        } break;
        default:
            mibs_log(MIBS_LL_ERROR, "unhandled case in print_expr()!\n");
            break;
    }
}

void print_stmts(Okapi_Stmts* stmts, size_t indent)
{
    for (size_t i = 0; i < stmts->count; i++) {
        Okapi_Stmt *stmt = stmts->items[i];
        if (!stmt) continue;
        switch (stmt->kind) {
            case OKAPI_SK_FUNCDEF: {
                mibs_log(MIBS_LL_INFO, "%*sfunction %s\n", indent*4, "", stmt->as.funcdef.name);
                for (size_t j = 0; j < stmt->as.funcdef.params->count; j++) {
                    size_t ptr_count = stmt->as.funcdef.params->items[j].type->ptr_count;
                    mibs_log(MIBS_LL_INFO, "param: %s, %.*s%s\n",
                            stmt->as.funcdef.params->items[j].name,
                            ptr_count, "************************************",
                            stmt->as.funcdef.params->items[j].type->base_name
                    );
                }
                print_stmts(stmt->as.funcdef.stmts, indent + 1);
            } break;
            case OKAPI_SK_VARDEF: {
                size_t ptr_count = stmt->as.vardef.type->ptr_count;
                mibs_log(MIBS_LL_INFO, "%*svardef %s %.*s%s\n", indent*4, "",
                        stmt->as.vardef.name,
                        ptr_count, "************************************",
                        stmt->as.vardef.type->base_name
                );
                print_expr(stmt->as.vardef.expr, indent + 1);
            } break;
        }
    }
}

int main(int argc, char** argv)
{
    const char* input_file = argv[1];
    argv += 1;
    Mibs_Options options = mibs_options(&allocator, argc-1, argv);
    const char* output_file = "out.c";
    Mibs_Option_Value* output_file_opt = map_get(&options, "o");
    if (output_file_opt) {
        if (!mibs_expect_option_kind(output_file_opt, "-o", MIBS_OV_STRING)) return 1;
        output_file = output_file_opt->data.string;
    }

    Mibs_Read_File_Result file_result = mibs_read_file(&allocator, input_file);
    if (!file_result.ok) return 1;
    Mibs_String_Builder file_content = file_result.value;
    
    Mibs_String_Builder full_file = {0};
    for (size_t i = 0; i < sizeof(lang_header)/sizeof(char); i++) {
        mibs_sb_append_char(&allocator, &full_file, lang_header[i]);
    }

    Okapi_Tokens tokens = okapi_tokenize(file_content.items, file_content.count);

    size_t j = 0;
    Okapi_Stmts *stmts = okapi_parse_stmts(tokens, &j);
    Mibs_String_Builder generated_c = okapi_generate_c(stmts);

    mibs_sb_append_cstr(&allocator, &full_file, generated_c.items);

    mibs_write_file(&allocator, output_file, full_file.items, full_file.count, MIBS_FK_TEXT, MIBS_FM_OVERWRITE);


    mibs_linear_destroy(&allocator);
    return 0;
}

