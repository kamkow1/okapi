#ifndef OKAPI_TOKENIZER_H
#define OKAPI_TOKENIZER_H

#include <stddef.h>
#include "mibs.h"

typedef enum {
    OKAPI_TK_IDENT = 0,
    OKAPI_TK_INT = 1,
    OKAPI_TK_FLOAT = 2,
    OKAPI_TK_STRING = 3,
    OKAPI_TK_CIMPORT = 4,
    OKAPI_TK_OSQBR = '[',
    OKAPI_TK_CSQBR = ']',
    OKAPI_TK_COLON = ':',
    OKAPI_TK_SEMICOLON = ';',
    OKAPI_TK_OCURLY = '{',
    OKAPI_TK_CCURLY = '}',
    OKAPI_TK_FUNCTION = 'f',
    OKAPI_TK_VARIABLE = 'v',
    OKAPI_TK_RETURN = 'r',
    OKAPI_TK_ASSIGN = '=',
    OKAPI_TK_ASTERISK = '*',
    OKAPI_TK_PLUS = '+',
    OKAPI_TK_MINUS = '-',
    OKAPI_TK_SLASH = '/',
    OKAPI_TK_OPAREN = '(',
    OKAPI_TK_CPAREN = ')',
} Okapi_Token_Kind;
#define OKAPI_STRING_TOKENS_COUNT 21
extern const char* string_tokens[];

typedef struct {
    size_t line;
    size_t column;
    Mibs_String_Builder text;
    Okapi_Token_Kind kind;
} Okapi_Token;

typedef Mibs_Da(Okapi_Token) Okapi_Tokens;

Okapi_Tokens okapi_tokenize(const char* text, size_t len);
bool okapi_is_single_char_token(char c);

#endif // OKAPI_TOKENIZER_H
