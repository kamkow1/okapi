#ifndef OKAPI_LANG_H
#define OKAPI_LANG_H

typedef void Void;
typedef int Int32;
typedef long Int64;
typedef char Char;
typedef char* String;
typedef float Float32;
typedef double Float64;

#endif // OKAPI_LANG_H
