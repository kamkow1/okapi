#include <stdlib.h>
#include "allocator_config.h"
#include "mibs.h"
#include "parser.h"
#include "tokenizer.h"
#include "allocator.h"

Okapi_Token* okapi_expect_token(Okapi_Token_Kind tk, Okapi_Tokens tokens, size_t* i)
{
    Okapi_Token *t = &tokens.items[*i];
    *i += 1;
    if (t->kind != tk) {
        mibs_log(MIBS_LL_INFO, "Unexpected token %s, expected %s\n",
                t->text.items, string_tokens[tk]);
        return NULL;
    }
    return t;
}

Okapi_Expr* okapi_parse_int_expr(Okapi_Tokens tokens, size_t* i)
{
    Okapi_Expr* expr = mibs_alloc(&allocator, sizeof(Okapi_Expr));
    expr->kind = OKAPI_EK_INT;

    Okapi_Token *token = okapi_expect_token(OKAPI_TK_INT, tokens, i);
    if (!token) return NULL;
    char* end = 0;
    expr->as.integer.value = strtoll(token->text.items, &end, 10);

    return expr;
}

Okapi_Expr* okapi_parse_float_expr(Okapi_Tokens tokens, size_t* i)
{
    Okapi_Expr* expr = mibs_alloc(&allocator, sizeof(Okapi_Expr));
    expr->kind = OKAPI_EK_FLOAT;

    Okapi_Token *token = okapi_expect_token(OKAPI_TK_FLOAT, tokens, i);
    if (!token) return NULL;
    char* end = 0;
    expr->as.floating_point.value = strtod(token->text.items, &end);

    return expr;
}

Okapi_Expr* okapi_parse_string_expr(Okapi_Tokens tokens, size_t* i)
{
    Okapi_Expr* expr = mibs_alloc(&allocator, sizeof(Okapi_Expr));
    expr->kind = OKAPI_EK_STRING;
    Okapi_Token *token = okapi_expect_token(OKAPI_TK_STRING, tokens, i);
    if (!token) return NULL;
    expr->as.string.value = token->text.items;
    return expr;
}

Okapi_Expr* okapi_parse_binop_expr(Okapi_Tokens tokens, size_t *i)
{
    Okapi_Expr* expr = mibs_alloc(&allocator, sizeof(Okapi_Expr));
    expr->kind = OKAPI_EK_BINOP;
    expr->as.binop.op = tokens.items[*i].text.items[0];
    *i += 1;

    Okapi_Expr* lhs = okapi_parse_expr(tokens, i);
    if (!lhs) return NULL;
    Okapi_Expr* rhs = okapi_parse_expr(tokens, i);
    if (!rhs) return NULL;
    expr->as.binop.lhs = lhs;
    expr->as.binop.rhs = rhs;

    return expr;
}

Okapi_Expr* okapi_parse_emph_expr(Okapi_Tokens tokens, size_t* i)
{
    Okapi_Expr* expr = mibs_alloc(&allocator, sizeof(Okapi_Expr));
    expr->kind = OKAPI_EK_EMPH;
    *i += 1; // (
    expr->as.emph.underlying = okapi_parse_expr(tokens, i);
    *i += 1; // )

    return expr;
}

Okapi_Exprs* okapi_parse_call_args(Okapi_Tokens tokens, size_t *i)
{
    Okapi_Exprs* args = (Okapi_Exprs*)mibs_alloc(&allocator, sizeof(Okapi_Exprs));
    while(true) {
        if (tokens.items[*i].kind == OKAPI_TK_CSQBR) {
            break;
        }

        Okapi_Expr *expr = okapi_parse_expr(tokens, i);
        if (!expr) return NULL;
        Okapi_Token* semicolon = okapi_expect_token(OKAPI_TK_SEMICOLON, tokens, i);
        if (!semicolon) return NULL;
        mibs_da_append(&allocator, args, expr);
    }
    return args;
}

Okapi_Expr* okapi_parse_call_expr(Okapi_Tokens tokens, size_t* i)
{
    Okapi_Expr* expr = mibs_alloc(&allocator, sizeof(Okapi_Expr));
    expr->kind = OKAPI_EK_CALL;
    Okapi_Token* name = okapi_expect_token(OKAPI_TK_IDENT, tokens, i);
    if (!name) return NULL;
    Okapi_Token* osqbr = okapi_expect_token(OKAPI_TK_OSQBR, tokens, i);
    if (!osqbr) return NULL;

    Okapi_Exprs* args = okapi_parse_call_args(tokens, i);
    if (!args) return NULL;
    expr->as.call.args = args;

    Okapi_Token* csqbr = okapi_expect_token(OKAPI_TK_CSQBR, tokens, i);
    if (!csqbr) return NULL;

    expr->as.call.name = name->text.items;

    return expr;
}

Okapi_Expr* okapi_parse_ident_expr(Okapi_Tokens tokens, size_t* i)
{
    Okapi_Expr* expr = mibs_alloc(&allocator, sizeof(Okapi_Expr));
    expr->kind = OKAPI_EK_IDENT;

    expr->as.ident.name = okapi_expect_token(OKAPI_TK_IDENT, tokens, i)->text.items;

    return expr;
}

Okapi_Expr* okapi_parse_expr_with_ident(Okapi_Tokens tokens, size_t* i)
{
    if (tokens.items[*i + 1].kind == OKAPI_TK_OSQBR) {
        return okapi_parse_call_expr(tokens, i);
    }
    return okapi_parse_ident_expr(tokens, i);
}

Okapi_Expr* okapi_parse_expr(Okapi_Tokens tokens, size_t *i)
{
    switch(tokens.items[*i].kind) {
        case OKAPI_TK_INT: return okapi_parse_int_expr(tokens, i);
        case OKAPI_TK_FLOAT: return okapi_parse_float_expr(tokens, i);
        case OKAPI_TK_STRING: return okapi_parse_string_expr(tokens, i);
        case OKAPI_TK_PLUS: case OKAPI_TK_MINUS: case OKAPI_TK_ASTERISK: case OKAPI_TK_SLASH:
             return okapi_parse_binop_expr(tokens, i);
        case OKAPI_TK_OPAREN: return okapi_parse_emph_expr(tokens, i); break;
        case OKAPI_TK_IDENT: return okapi_parse_expr_with_ident(tokens, i); break;
        default:
            mibs_log(MIBS_LL_ERROR, "%zu:%zu %s %s: Unhandled case in okapi_parse_stmt()!\n",
                    tokens.items[*i].line,
                    tokens.items[*i].column,
                    tokens.items[*i].text.items,
                    string_tokens[tokens.items[*i].kind]
            );
            break;
    }
    return NULL;
}

Okapi_Type* okapi_parse_type(Okapi_Tokens tokens, size_t *i)
{
    Okapi_Type* type = mibs_alloc(&allocator, sizeof(Okapi_Type));

    while(tokens.items[*i].kind == OKAPI_TK_ASTERISK) {
        Okapi_Token* asterisk = okapi_expect_token(OKAPI_TK_ASTERISK, tokens, i);
        if (!asterisk) return NULL;
        type->ptr_count += 1;
    }
    Okapi_Token* base_name = okapi_expect_token(OKAPI_TK_IDENT, tokens, i);
    if (!base_name) return NULL;
    type->base_name = base_name->text.items;
    return type;
}

Okapi_Func_Params* okapi_parse_func_params(Okapi_Tokens tokens, size_t *i)
{
    Okapi_Func_Params *params = mibs_alloc(&allocator, sizeof(Okapi_Func_Params));

    while(true) {
        if (tokens.items[*i].kind == OKAPI_TK_CSQBR) {
            break;
        }

        Okapi_Token* name = okapi_expect_token(OKAPI_TK_IDENT, tokens, i);
        if (!name) return NULL;
        Okapi_Token* colon = okapi_expect_token(OKAPI_TK_COLON, tokens, i);
        if (!colon) return NULL;
        Okapi_Type* type = okapi_parse_type(tokens, i);
        if (!type) return NULL;
        Okapi_Token* semicolon = okapi_expect_token(OKAPI_TK_SEMICOLON, tokens, i);
        if (!semicolon) return NULL;

        Okapi_Func_Param param = {
            .name = name->text.items,
            .type = type,
        };
        mibs_da_append(&allocator, params, param);
    }

    return params;
}

Okapi_Stmt* okapi_parse_funcdef_stmt(Okapi_Tokens tokens, size_t *i)
{
    Okapi_Stmt* stmt = mibs_alloc(&allocator, sizeof(Okapi_Stmt));
    stmt->kind = OKAPI_SK_FUNCDEF;

    Okapi_Token* f = okapi_expect_token(OKAPI_TK_FUNCTION, tokens, i);
    if (!f) return NULL;
    Okapi_Token* name = okapi_expect_token(OKAPI_TK_IDENT, tokens, i);
    if (!name) return NULL;
    Okapi_Token* osqbr = okapi_expect_token(OKAPI_TK_OSQBR, tokens, i);
    if (!osqbr) return NULL;
    Okapi_Func_Params *params = okapi_parse_func_params(tokens, i);
    if (!params) return NULL;
    Okapi_Token* csqbr = okapi_expect_token(OKAPI_TK_CSQBR, tokens, i);
    if (!csqbr) return NULL;
    Okapi_Type* return_type = okapi_parse_type(tokens, i);
    if (!return_type) return NULL;
    Okapi_Token* ocurly = okapi_expect_token(OKAPI_TK_OCURLY, tokens, i);
    if (!ocurly) return NULL;
    Okapi_Stmts* stmts = okapi_parse_stmts(tokens, i);
    if (!stmts) return NULL;
    Okapi_Token* ccurly = okapi_expect_token(OKAPI_TK_CCURLY, tokens, i);
    if (!ccurly) return NULL;
    
    stmt->as.funcdef.name = name->text.items;
    stmt->as.funcdef.params = params;
    stmt->as.funcdef.stmts = stmts;
    stmt->as.funcdef.return_type = return_type;

    return stmt;
}

Okapi_Stmt* okapi_parse_vardef_stmt(Okapi_Tokens tokens, size_t *i)
{
    Okapi_Stmt* stmt = mibs_alloc(&allocator, sizeof(Okapi_Stmt));
    stmt->kind = OKAPI_SK_VARDEF;

    Okapi_Token* v = okapi_expect_token(OKAPI_TK_VARIABLE, tokens, i);
    if (!v) return NULL;
    Okapi_Token* name = okapi_expect_token(OKAPI_TK_IDENT, tokens, i);
    if (!name) return NULL;
    Okapi_Token* colon = okapi_expect_token(OKAPI_TK_COLON, tokens, i);
    if (!colon) return NULL;
    Okapi_Type* type = okapi_parse_type(tokens, i);
    if (!type) return NULL;

    stmt->as.vardef.name = name->text.items;
    stmt->as.vardef.type = type;

    if (tokens.items[*i].kind == OKAPI_TK_ASSIGN) {
        Okapi_Token* assign = okapi_expect_token(OKAPI_TK_ASSIGN, tokens, i);
        if (!assign) return NULL;
        Okapi_Expr* expr = okapi_parse_expr(tokens, i);
        if (!expr) return NULL;
        stmt->as.vardef.expr = expr;
    }

    return stmt;
}

Okapi_Stmt* okapi_parse_return_stmt(Okapi_Tokens tokens, size_t *i)
{
    Okapi_Stmt* stmt = mibs_alloc(&allocator, sizeof(Okapi_Stmt));
    stmt->kind = OKAPI_SK_RETURN;

    Okapi_Token* r = okapi_expect_token(OKAPI_TK_RETURN, tokens, i);
    if (!r) return NULL;

    Okapi_Expr* expr = okapi_parse_expr(tokens, i);
    if (!expr) return NULL;

    stmt->as.retrn.expr = expr;

    return stmt;
}

Okapi_Stmt* okapi_parse_expr_stmt(Okapi_Tokens tokens, size_t *i)
{
    Okapi_Stmt* stmt = mibs_alloc(&allocator, sizeof(Okapi_Stmt));
    stmt->kind = OKAPI_SK_EXPR;

    Okapi_Expr* expr = okapi_parse_expr(tokens, i);
    if (!expr) return NULL;
    stmt->as.expr.expr = expr;

    return stmt;
}

Okapi_Stmt* okapi_parse_cimport_stmt(Okapi_Tokens tokens, size_t *i)
{
    Okapi_Stmt* stmt = mibs_alloc(&allocator, sizeof(Okapi_Stmt));
    stmt->kind = OKAPI_SK_CIMPORT;

    Okapi_Token* c = okapi_expect_token(OKAPI_TK_CIMPORT, tokens, i);
    if (!c) return NULL;
    Okapi_Expr* expr = okapi_parse_expr(tokens, i);
    if (!expr) return NULL;

    stmt->as.cimport.path = expr->as.string.value;

    return stmt;
}

Okapi_Stmt* okapi_parse_stmt(Okapi_Tokens tokens, size_t *i)
{
    switch(tokens.items[*i].kind) {
        case OKAPI_TK_FUNCTION: return okapi_parse_funcdef_stmt(tokens, i);
        case OKAPI_TK_VARIABLE: return okapi_parse_vardef_stmt(tokens, i);
        case OKAPI_TK_RETURN: return okapi_parse_return_stmt(tokens, i);
        case OKAPI_TK_CIMPORT: return okapi_parse_cimport_stmt(tokens, i);
        default: return okapi_parse_expr_stmt(tokens, i);
    }
    return NULL;
}

Okapi_Stmts *okapi_parse_stmts(Okapi_Tokens tokens, size_t *i)
{
    Okapi_Stmts *stmts = mibs_alloc(&allocator, sizeof(Okapi_Stmts));

    while(*i < tokens.count && tokens.items[*i].kind != OKAPI_TK_CCURLY) {
        Okapi_Stmt* stmt = okapi_parse_stmt(tokens, i);
        mibs_da_append(&allocator, stmts, stmt);
    }
    return stmts;
}

